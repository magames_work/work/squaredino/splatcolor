﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class VibrateOnRotate : MonoBehaviour
    {
        [Range(0, 1)]
        public float interval;
        private Quaternion Qdiff;
        VibroManager vm;

        void Start()
        {
            vm = FindObjectOfType<VibroManager>();
            Qdiff = transform.rotation;
        }

        void Update()
        {
            if (transform.rotation.w > Qdiff.w + interval |
                transform.rotation.x > Qdiff.x + interval |
                transform.rotation.y > Qdiff.y + interval |
                transform.rotation.z > Qdiff.z + interval) OnRotation();

            else if (transform.rotation.w < Qdiff.w - interval |
                transform.rotation.x < Qdiff.x - interval |
                transform.rotation.y < Qdiff.y - interval |
                transform.rotation.z < Qdiff.z - interval) OnRotation();
        }

        private void OnRotation()
        {
            Qdiff = transform.rotation;
            vm.TriggerLightImpact();
            //print("Light Vibrate");
        }
    }
}
