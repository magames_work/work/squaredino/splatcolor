﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] Brush brush;
    [SerializeField] GameObject particlePrefab;
    Rigidbody rg;

    private ColorManager colormanager;
    private UIManager ui;
    private PaintThings gameplay;
    private VibroManager vm;

    void Start()
    {
        Destroy(gameObject, 3f);

        gameplay = FindObjectOfType<PaintThings>();
        vm = FindObjectOfType<VibroManager>();
        ui = FindObjectOfType<UIManager>();
        colormanager = FindObjectOfType<ColorManager>();
        ParticleSystem.MainModule settings = particlePrefab.GetComponent<ParticleSystem>().main;
        settings.startColor = gameplay.color;

    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);

        var paintTarget = collision.gameObject.GetComponent<PaintTarget>();
        PaintTarget.PaintObject(paintTarget, collision.contacts[0].point, collision.contacts[0].normal, brush);
        var particle = Instantiate(particlePrefab, transform.position, Camera.main.transform.rotation);
        Destroy(particle, 2f);

        if (!collision.transform.gameObject.GetComponent<SetColor>().IsModel)
        {
            if (!collision.transform.gameObject.GetComponent<SetColor>().isPainted)
            {
                collision.transform.gameObject.GetComponent<MeshRenderer>().material.color = colormanager.ThemeColors[colormanager.Theme].colors[collision.transform.gameObject.GetComponent<SetColor>().ColorNum];
                collision.transform.gameObject.GetComponent<Renderer>().sharedMaterial.SetColor("_SplatColor1", colormanager.ThemeColors[colormanager.Theme].colors[collision.transform.gameObject.GetComponent<SetColor>().ColorNum]);
                gameplay.paintCount++;
                collision.transform.GetComponent<SetColor>().isPainted = true;
                ui.OnPaint();
            }
        }
        else
        {
            collision.transform.gameObject.GetComponent<MeshRenderer>().material.color = colormanager.ThemeColors[colormanager.Theme].ModelColor;
            collision.transform.gameObject.GetComponent<Renderer>().sharedMaterial.SetColor("_SplatColor1", colormanager.ThemeColors[colormanager.Theme].ModelColor);
        }
    }
}
