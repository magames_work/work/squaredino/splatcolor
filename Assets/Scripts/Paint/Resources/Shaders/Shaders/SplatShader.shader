﻿Shader "Splats/SplatShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpTex ("Normal", 2D) = "bump" {}
		_SplatTileNormalTex ("Splat Tile Normal", 2D) = "bump" {}
		[HideInInspector]_SplatTileBump ("Splat Tile Bump", Range(0,10)) = 1.0
		[HideInInspector]_SplatEdgeBump ("Splat Edge Bump", Range(0,10)) = 1.0
		[HideInInspector]_SplatEdgeBumpWidth ("Splat Edge Bump Width", Range(0,10)) = 1.0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpTex;
		sampler2D _SplatTileNormalTex;
		float _SplatEdgeBump;
		float _SplatEdgeBumpWidth;
		float _SplatTileBump;

		sampler2D _SplatTex;
		float4 _SplatTex_TexelSize;
		sampler2D _WorldTangentTex;
		sampler2D _WorldBinormalTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_SplatTex;
			float3 worldNormal;
			float3 worldTangent;
			float3 worldBinormal;
			float3 worldPos;
			INTERNAL_DATA
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		
		static const float _Clip = 0.5;

		float3x3 cotangent_frame( float3 N, float3 p, float2 uv )
		{
		    float3 dp1 = ddx( p );
		    float3 dp2 = ddy( p );
		    float2 duv1 = ddx( uv );
		    float2 duv2 = ddy( uv );
		 
		    float3 dp2perp = cross( dp2, N );
		    float3 dp1perp = cross( N, dp1 );
		    float3 T = dp2perp * duv1.x + dp1perp * duv2.x;
		    float3 B = dp2perp * duv1.y + dp1perp * duv2.y;
		 
		    float invmax = rsqrt( max( dot(T,T), dot(B,B) ) );
		    float3 TinvMax = normalize(T * invmax);
		    float3 BinvMax =  normalize(B * invmax);
		    return float3x3(float3( TinvMax.x, BinvMax.x, N.x ), float3( TinvMax.y, BinvMax.y, N.y ), float3( TinvMax.z, BinvMax.z, N.z ));
		}

		half3 perturb_normal( float3 localNormal, float3 N, float3 V, float2 uv )
		{
			float3x3 TBN = cotangent_frame( N, -V, uv );
			return normalize( mul( TBN, localNormal ) );
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			
			float4 splatSDF = tex2D (_SplatTex, IN.uv2_SplatTex);
			float4 splatSDFx = tex2D (_SplatTex, IN.uv2_SplatTex + float2(_SplatTex_TexelSize.x,0) );
			float4 splatSDFy = tex2D (_SplatTex, IN.uv2_SplatTex + float2(0,_SplatTex_TexelSize.y) );

			half splatDDX = length( ddx(IN.uv2_SplatTex * _SplatTex_TexelSize.zw) );
			half splatDDY = length( ddy(IN.uv2_SplatTex * _SplatTex_TexelSize.zw) );
			half clipDist = sqrt( splatDDX * splatDDX + splatDDY * splatDDY );
			half clipDistHard = max( clipDist * 0.01, 0.01 );
			half clipDistSoft = 0.01 * _SplatEdgeBumpWidth;

			float4 splatMask = smoothstep( ( _Clip - 0.01 ) - clipDistHard, ( _Clip - 0.01 ) + clipDistHard, splatSDF );
			float splatMaskTotal = max( max( splatMask.x, splatMask.y ), max( splatMask.z, splatMask.w ) );

			float4 splatMaskInside = smoothstep( _Clip - clipDistSoft, _Clip + clipDistSoft, splatSDF );
			splatMaskInside = max( max( splatMaskInside.x, splatMaskInside.y ), max( splatMaskInside.z, splatMaskInside.w ) );

			float4 offsetSplatX = splatSDF - splatSDFx;
			float4 offsetSplatY = splatSDF - splatSDFy;

			float2 offsetSplat = lerp( float2(offsetSplatX.x,offsetSplatY.x), float2(offsetSplatX.y,offsetSplatY.y), splatMask.y );
			offsetSplat = lerp( offsetSplat, float2(offsetSplatX.z,offsetSplatY.z), splatMask.z );
			offsetSplat = lerp( offsetSplat, float2(offsetSplatX.w,offsetSplatY.w), splatMask.w );
			offsetSplat = normalize( float3( offsetSplat, 0.0001) ).xy;
			offsetSplat = offsetSplat * ( 1.0 - splatMaskInside ) * _SplatEdgeBump;

			float2 splatTileNormalTex = tex2D( _SplatTileNormalTex, IN.uv2_SplatTex * 10.0 ).xy;
			offsetSplat += ( splatTileNormalTex.xy - 0.5 ) * _SplatTileBump  * 0.2;

			#if 0
				float3 worldNormal = WorldNormalVector (IN, float3(0,0,1) );
				float3 offsetSplatLocal2 = normalize( float3( offsetSplat, sqrt( 1.0 - saturate( dot( offsetSplat, offsetSplat ) ) ) ) );
				float3 offsetSplatWorld = perturb_normal( offsetSplatLocal2, worldNormal, normalize( IN.worldPos - _WorldSpaceCameraPos ), IN.uv2_SplatTex );
			#else
				float3 worldTangentTex = tex2D ( _WorldTangentTex, IN.uv2_SplatTex ).xyz * 2.0 - 1.0;
				float3 worldBinormalTex = tex2D ( _WorldBinormalTex, IN.uv2_SplatTex ).xyz * 2.0 - 1.0;

				float3 offsetSplatWorld = offsetSplat.x * worldTangentTex + offsetSplat.y * worldBinormalTex;
			#endif

			float3 worldTangent = WorldNormalVector (IN, float3(1,0,0) );
			float3 worldBinormal = WorldNormalVector (IN, float3(0,1,0) );

			float2 offsetSplatLocal = 0;
			offsetSplatLocal.x = dot( worldTangent, offsetSplatWorld );
			offsetSplatLocal.y = dot( worldBinormal, offsetSplatWorld );

			float4 normalMap = tex2D( _BumpTex, IN.uv_MainTex );
			normalMap.xyz = UnpackNormal( normalMap );
			float3 tanNormal = normalMap.xyz;

			tanNormal.xy += offsetSplatLocal * splatMaskTotal;
			tanNormal = normalize( tanNormal );

			float4 MainTex = tex2D (_MainTex, IN.uv_MainTex );
			fixed4 c = MainTex * _Color;

			c.xyz = lerp( c.xyz, float3(1.0,0.5,0.0), splatMask.x );
			c.xyz = lerp( c.xyz, float3(1.0,0.0,0.0), splatMask.y );
			c.xyz = lerp( c.xyz, float3(0.0,1.0,0.0), splatMask.z );
			c.xyz = lerp( c.xyz, float3(0.0,0.0,1.0), splatMask.w );
			
			o.Albedo = c.rgb;
			o.Normal = tanNormal;
			o.Metallic = _Metallic;
			o.Smoothness = lerp( _Glossiness, 0.7, splatMaskTotal );
			o.Alpha = c.a;

		}
		ENDCG
	} 
	FallBack "Diffuse"
}
