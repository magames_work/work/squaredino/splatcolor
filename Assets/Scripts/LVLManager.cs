﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LVLManager : MonoBehaviour
{
    //Classes
    private PaintThings gameplay;
    private UIManager uimanager;
    private VibroManager vibromanager;
    private ColorManager colormanager;
    
    //Variables
    [HideInInspector]
    public int nextLVL;
    [HideInInspector]
    public int currentlvl;
    [HideInInspector]
    public bool Debug;

    public int LvlAfterEnd;
    private int CountNextLVL;
    private GameObject currentObj, NextObj;
    

    //Lists
    public List<Lvls> Levels = new List<Lvls>();
    [Serializable]
    public class Lvls
    {
        public GameObject paintObj;
        public int ThemeOrder;
        [HideInInspector]
        public int paintNeed;
    }

    private void Start()
    {
        uimanager = FindObjectOfType<UIManager>();
        gameplay = FindObjectOfType<PaintThings>();
        vibromanager = FindObjectOfType<VibroManager>();
        colormanager = FindObjectOfType<ColorManager>();

        if (!PlayerPrefs.HasKey("Saved LVL")) currentlvl = 0;
        else currentlvl = PlayerPrefs.GetInt("Saved LVL");

        CalculateNextLvl();
        uimanager.LvlCountUpdate();

        FillThemeOrder();

        currentObj = Instantiate(Levels[currentlvl].paintObj, new Vector3(0, 0, 0), Levels[currentlvl].paintObj.transform.rotation) as GameObject;
        PaintCount();
    }

    private void Update()
    {
        CalculateNextLvl();
        if (gameplay.PaintCount >= Levels[currentlvl].paintNeed)
        {
            NextLvl();
        }
    }

    public void NextLvl()
    {
        if (currentlvl + 1 < Levels.Count)
        {
            currentlvl++;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            uimanager.LvlCleared();
            gameplay.ResetPaintCount();
        }
        else if (currentlvl + 1 >= Levels.Count)
        {
            currentlvl = LvlAfterEnd;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            uimanager.GameComplited();
            vibromanager.TriggerSuccess();
            gameplay.ResetPaintCount();
        }
    }

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();
    }

    public void StartSlide()
    {
        StartCoroutine(Slide());
    }

    public IEnumerator Slide()
    {
        iTween.MoveTo(currentObj, -Camera.main.transform.right * 1300, 2f);
        NextObj = Instantiate(Levels[currentlvl].paintObj, Camera.main.transform.right * 1300, Levels[nextLVL].paintObj.transform.rotation) as GameObject;

        iTween.MoveTo(NextObj, new Vector3(0, 0, 0), 2f);

        GameObject preObj = currentObj;
        currentObj = NextObj;

        PaintCount();

        yield return new WaitForSeconds(2);
        Destroy(preObj.gameObject);
        uimanager.GameplayEnable();
    }

    public void SetCustomLvl()
    {
        uimanager.SetCustomLvL();
    }

    public void CalculateNextLvl()
    {
        CountNextLVL = currentlvl + 1;
        if (CountNextLVL < Levels.Count) nextLVL = currentlvl + 1;
        else nextLVL = LvlAfterEnd;

    }

    private void PaintCount()
    {
        var PaintCount = currentObj.GetComponentsInChildren<SetColor>();
        int modelCount = 0;

        for (int i = 0; PaintCount.Length > i; i++)
        {
            if (currentObj.GetComponentsInChildren<SetColor>()[i].IsModel == true)
            {
                modelCount++;
            }
        }

        if (currentObj.GetComponentInChildren<SetColor>().IsModel == true) Levels[currentlvl].paintNeed = PaintCount.Length - modelCount;
        else Levels[currentlvl].paintNeed = PaintCount.Length;
    }

    private void FillThemeOrder()
    {
        int themesCount = 0;
        int interval = colormanager.ThemesInterval;
        Levels[0].ThemeOrder = themesCount;
        for (int i = colormanager.ThemesInterval; Levels.Capacity > i; i++)
        {
            if (i >= interval)
            {
                themesCount++;
                interval += colormanager.ThemesInterval;
            }
            if (themesCount == colormanager.ThemeColors.Capacity) themesCount = 0;
            else Levels[i].ThemeOrder = themesCount;
        }
    }
}


