﻿using Core.Scripts.Framework.Utils;
using UnityEngine;

namespace Scripts
{
    public class CameraRotateAround : MonoBehaviour
    {
        [Range(0f, 1f)] [SerializeField] private float rotationTime = .5f;
        [Range(1f, 300f)] [SerializeField] private float rotationSpeed = 4f;
	
        private Vector3 _startCursorPoint = Vector3.zero;
        private float _rotationX;
        private float _targetRotationX;
        private float _rotationZ;
        private float _targetRotationZ;
        private bool _canControll = true;
        private Quaternion _targetRotation;

        private void OnEnable()
        {
            GameInput.OnPointerMove += OnPointerMove;
            GameInput.OnPointerUp += OnPointerUp;
            _targetRotation = transform.rotation;
//        BuildingInfo.OnRotateCameraToThisAngle += OnRotateCameraToThisAngle;
        }
	
        private void OnDisable()
        {
            GameInput.OnPointerMove -= OnPointerMove;
            GameInput.OnPointerUp -= OnPointerUp;
//        BuildingInfo.OnRotateCameraToThisAngle -= OnRotateCameraToThisAngle;
        }

//    private void OnRotateCameraToThisAngle(float obj)
//    {
//        var diff = targetRotation.eulerAngles.y - obj;
//        
//        targetRotation = Quaternion.AngleAxis(diff, Vector3.up);
//    }

        private void OnPointerUp()
        {
            if (!_canControll) return;
            _startCursorPoint = Vector3.zero;
        }

        private void OnPointerMove()
        {
            if (!_canControll) return;
            if (Input.touchCount > 1) return;
        
            if (_startCursorPoint == Vector3.zero) _startCursorPoint = Input.mousePosition;
		
            var cursorPosition = Input.mousePosition;
            var mouseDisplacement = cursorPosition - _startCursorPoint;

            mouseDisplacement.x /= Screen.width;
            mouseDisplacement.y /= Screen.height;

            var newX = rotationSpeed * mouseDisplacement.x;
            var newY = rotationSpeed * mouseDisplacement.y;
            
//            ControllType1(newX, newY);
            ControllType2(newX, newY);

            _startCursorPoint = cursorPosition;
        }

        private void Update()
        {
//            ControlTypeUpdate1();
            ControlTypeUpdate2();
            transform.position = transform.forward * -500;
        }

        private void ControllType1(float newX, float newY)
        {
            _targetRotationX += newX;
            _targetRotationZ += newY;
        }

        private void ControlTypeUpdate1()
        {
            _rotationX = Mathf.Lerp(_rotationX, _targetRotationX, 10f * rotationTime * Time.deltaTime);
            _rotationZ = Mathf.Lerp(_rotationZ, _targetRotationZ, 10f * rotationTime * Time.deltaTime);
            transform.Rotate(transform.up, _targetRotationX - _rotationX, Space.World);
            transform.Rotate(-transform.right, _targetRotationZ - _rotationZ, Space.World);
        }
        
        private void ControllType2(float newX, float newY)
        {
            var yaw = Quaternion.AngleAxis(newX, transform.up);
            var pitch = Quaternion.AngleAxis(newY, -transform.right);
            
            _targetRotation = yaw * pitch * _targetRotation;
        }
        
        private void ControlTypeUpdate2()
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, _targetRotation,
                10f * rotationTime * Time.deltaTime);
        }

        public void ActivateControl(bool flag)
        {
            _canControll = flag;
        }


    }
}