﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SetColor : MonoBehaviour
{

#if UNITY_EDITOR
    [CustomEditor(typeof(SetColor))]
    public class SetColorGUI : Editor
    {
        SetColor setColor;
        ColorManager colormanager;

        public void OnEnable()
        {
            setColor = (SetColor)target;
            colormanager = FindObjectOfType<ColorManager>();
        }

        public override void OnInspectorGUI()
        {
            setColor.IsModel = GUILayout.Toggle(setColor.IsModel, "IsModel");
            if (!setColor.IsModel)
                setColor.ColorNum = EditorGUILayout.IntSlider("Set Color Number", setColor.ColorNum, 0, colormanager.ThemeColors[colormanager.Theme].colors.Capacity);
        }
    }
#endif

    [HideInInspector]
    public bool IsModel;
    [HideInInspector]
    public int ColorNum;
    [HideInInspector]
    public bool isPainted;

    private ColorManager colormanager;

    public void Start()
    {
        colormanager = FindObjectOfType<ColorManager>();
        this.gameObject.GetComponent<MeshRenderer>().material.color = colormanager.ThemeColors[colormanager.Theme].DefaultColor;
    }
}
