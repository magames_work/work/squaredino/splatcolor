﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(LVLManager))]
public class GameGUI : Editor
{
    LVLManager lvlmanager;

    public override void OnInspectorGUI()
    {
        lvlmanager = FindObjectOfType<LVLManager>();
        lvlmanager.Debug = GUILayout.Toggle(lvlmanager.Debug, "Set Level");

        if (lvlmanager.Debug)
        {
            lvlmanager.currentlvl = EditorGUILayout.IntSlider("Level", lvlmanager.currentlvl, 0, lvlmanager.Levels.Capacity - 1);
            if (GUILayout.Button("Set"))
            {
                lvlmanager.SetCustomLvl();
            }
        }
        base.OnInspectorGUI();
        if(GUILayout.Button("Reset game"))
        {
            lvlmanager.ResetGame();
        }
    }
}
#endif
