﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Scripts;

public class UIManager : MonoBehaviour
{
    //Classes
    private LVLManager lvlManager;
    private PaintThings gameplay;
    private CameraRotateAround camerarotate;

    //UI
    [Header("TextMeshPro")]
    public TextMeshProUGUI NextLVL;
    public TextMeshProUGUI LvlCounter, lvlcleared;

    [Header("GameObjects")]
    public GameObject instr;
    public GameObject LvlClearedPanel, shopfreespace, nextlvlbutton;

    [Header("Rect Transform")]
    public RectTransform ShopButton;
    public RectTransform VibroButton, ShopPanel, Tip;

    [Header("ProgressBar")]
    public Slider progressBar;

    [Header("Particle System")]
    public ParticleSystem FireworkParticleSystem;

    private bool IsMenuSwiped;

    private void Start()
    {
        lvlManager = FindObjectOfType<LVLManager>();
        gameplay = FindObjectOfType<PaintThings>();
        camerarotate = FindObjectOfType<CameraRotateAround>();
        instr.SetActive(true);
    }

    public void LvlCountUpdate()
    {
        LvlCounter.text = lvlManager.currentlvl.ToString();
        NextLVL.text = lvlManager.nextLVL.ToString();
    }

    public void OnPaint()
    {
        progressBar.maxValue = lvlManager.Levels[lvlManager.currentlvl].paintNeed;
        DOTween.To(() => progressBar.value, x => progressBar.value = x, gameplay.PaintCount, .5f);
    }

    public void ResetProgressBar()
    {
        DOTween.To(() => progressBar.value, x => progressBar.value = x, 0, .5f);
    }

    public void SetCustomLvL()
    {
        LvlCountUpdate();
        ResetProgressBar();
        lvlManager.StartSlide();
    }

    public void LvlCleared()
    {
        StartCoroutine(Lvlcleared());
    }

    IEnumerator Lvlcleared()
    {
        FireworkParticleSystem.Play();
        DragButtonsOn();
        camerarotate.enabled = false;
        gameplay.enabled = false;

        LvlClearedPanel.GetComponent<Button>().enabled = false;
        lvlcleared.text = "Level Cleared!";

        yield return new WaitForSeconds(1.4f);
        LvlClearedPanel.SetActive(true);
        LvlClearedPanel.GetComponent<Button>().enabled = true;
        nextlvlbutton.SetActive(true);
    }

    public void GameComplited()
    {
        StartCoroutine(gamecomplited());
    }

    IEnumerator gamecomplited()
    {
        FireworkParticleSystem.Play();
        DragButtonsOn();
        camerarotate.enabled = false;
        gameplay.enabled = false;

        LvlClearedPanel.GetComponent<Button>().enabled = false;
        lvlcleared.text = "Level Cleared!";

        yield return new WaitForSeconds(1.4f);
        LvlClearedPanel.SetActive(true);
        LvlClearedPanel.GetComponent<Button>().enabled = true;
        nextlvlbutton.SetActive(true);
    }

    public void DragButtonsOut()
    {
        lvlcleared.text = "";
        ShopButton.DOAnchorPosX(-200, 1);
        VibroButton.DOAnchorPosX(200, 1);
    }

    public void DragButtonsOn()
    {
        ShopButton.DOAnchorPosX(100, .5f);
        VibroButton.DOAnchorPosX(-100, .5f);
    }

    public void DragPanelOn()
    {
        ShopPanel.DOAnchorPos(Vector2.zero, 0.5f);
        shopfreespace.SetActive(true);
    }

    public void DragPanelOut()
    {
        StartCoroutine(SwipePanelOut());
    }

    IEnumerator SwipePanelOut()
    {
        ShopPanel.DOAnchorPos(new Vector2(0, -1000), 0.5f);
        yield return new WaitForSeconds(0.5f);
        shopfreespace.SetActive(false);
    }

    public void LvlEnter()
    {
        if (!IsMenuSwiped)
        {
            StartCoroutine(SwipePanelOut());
            DragButtonsOut();
            instr.SetActive(false);
            IsMenuSwiped = true;
        }
    }

    public void LvlEscape()
    {
        IsMenuSwiped = false;
    }

    public void GameplayEnable()
    {
        camerarotate.enabled = true;
        gameplay.enabled = true;
    }
}
