﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ColorManager : MonoBehaviour
{
    private LVLManager lvlManager;
    private UIManager ui;

    [HideInInspector]
    public int ThemesInterval;
    [HideInInspector]
    public bool SetDefaultTheme;
    [HideInInspector]
    public int Theme = 0;

#if UNITY_EDITOR
    [CustomEditor(typeof(ColorManager))]
    public class ColorManagerGUI : Editor
    {
        public override void OnInspectorGUI()
        {
            ColorManager colormanager = FindObjectOfType<ColorManager>();

            colormanager.SetDefaultTheme = GUILayout.Toggle(colormanager.SetDefaultTheme, "SetDefaultTheme");
            colormanager.ThemesInterval = EditorGUILayout.IntSlider("Themes Interval", colormanager.ThemesInterval, 1, colormanager.ThemeColors.Capacity);

            if (colormanager.SetDefaultTheme)
                colormanager.Theme = EditorGUILayout.IntSlider("Theme", colormanager.Theme, 0, colormanager.ThemeColors.Capacity);
            base.OnInspectorGUI();
        }
    }
#endif

    public List<Themes> ThemeColors = new List<Themes>();
    [Serializable]
    public class Themes
    {
        public string name;
        public Color DefaultColor;
        public Color ModelColor;
        public Color BackgroundColor;
        public List<Color> colors = new List<Color>();
    }

    private void Start()
    {
        lvlManager = FindObjectOfType<LVLManager>();
        if (!PlayerPrefs.HasKey("Saved Theme") & !SetDefaultTheme) Theme = 0;
        else if (!SetDefaultTheme) Theme = PlayerPrefs.GetInt("Saved Theme");
    }

    public void Update()
    {
        if (!SetDefaultTheme)
        {
            Theme = lvlManager.Levels[lvlManager.currentlvl].ThemeOrder;
            PlayerPrefs.SetInt("Saved Theme", lvlManager.Levels[lvlManager.currentlvl].ThemeOrder);
        }
    }

}
