﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Scripts.Framework.Utils;
using UnityEngine;

public class PaintThings : MonoBehaviour
{
    SetColor sc;
    ColorManager colormanager;
    LVLManager lvlmanager;
    UIManager ui;
    VibroManager vibromanager;
    GameInput gi;

    public static event Action OnPointerDown = delegate { };
    public static event Action OnPointerUp = delegate { };
    public static event Action OnPointerMove = delegate { };

    public enum Gamemod { Gamemode1, Gamemode2 };
    public Gamemod gamemod;

    public GameObject bullet;

    public float PaintCount { get { return paintCount; } }
    [HideInInspector]
    public int paintCount;
    [HideInInspector]
    public Color color;

    private bool c1 = false;


    // Start is called before the first frame update
    void Start()
    {
        sc = FindObjectOfType<SetColor>();
        lvlmanager = FindObjectOfType<LVLManager>();
        vibromanager = FindObjectOfType<VibroManager>();
        ui = FindObjectOfType<UIManager>();
        colormanager = FindObjectOfType<ColorManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction * 1000);
        if (Physics.Raycast(ray, out hit))
        {
            if (gamemod == Gamemod.Gamemode1)
            {
                if (Input.GetMouseButtonDown(0) & !hit.transform.GetComponent<SetColor>().isPainted)
                {
                    if (c1 == false)
                    {
                        if (!hit.transform.gameObject.GetComponent<SetColor>().IsModel)
                            hit.transform.gameObject.GetComponent<MeshRenderer>().material.color = colormanager.ThemeColors[colormanager.Theme].colors[hit.transform.gameObject.GetComponent<SetColor>().ColorNum];
                        else
                            hit.transform.gameObject.GetComponent<MeshRenderer>().material.color = colormanager.ThemeColors[colormanager.Theme].ModelColor;

                        hit.transform.GetComponent<SetColor>().isPainted = true;
                        paintCount++;
                        ui.OnPaint();
                        vibromanager.TriggerMediumImpact();
                        c1 = false;
                    }
                }
            }
            if (gamemod == Gamemod.Gamemode2)
            {
                if (Input.GetMouseButtonUp(0) & c1 == false)
                {
                     GameObject dye = Instantiate(bullet);
                    if (!hit.transform.gameObject.GetComponent<SetColor>().IsModel)
                    {
                        color = colormanager.ThemeColors[colormanager.Theme].colors[hit.transform.gameObject.GetComponent<SetColor>().ColorNum];
                        bullet.GetComponent<MeshRenderer>().sharedMaterial.color = colormanager.ThemeColors[colormanager.Theme].colors[hit.transform.gameObject.GetComponent<SetColor>().ColorNum];
                    }
                    else
                    {
                        color = colormanager.ThemeColors[colormanager.Theme].ModelColor;
                        bullet.GetComponent<MeshRenderer>().sharedMaterial.color = colormanager.ThemeColors[colormanager.Theme].ModelColor;
                    }
                    iTween.ScaleTo(dye, new Vector3(30, 30, 30), 0);
                    dye.transform.position = Camera.main.transform.position;
                    dye.layer = 2;
                    iTween.MoveTo(dye, hit.point, 1f);

                    c1 = false;
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            c1 = false;
        }
    }

    public void ResetPaintCount()
    {
        paintCount = 0;
    }

    public void PointerMove()
    {
        c1 = true;
        OnPointerMove();
    }

}
