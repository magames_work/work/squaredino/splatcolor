﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Gameplay : MonoBehaviour
{
    VibroManager vibromanager;
    UIManager uimanager;

    public int ClickCount { get { return clickCount; } }
    int clickCount;

    private void Awake()
    {
        vibromanager = FindObjectOfType<VibroManager>();
        uimanager = FindObjectOfType<UIManager>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            vibromanager.TriggerSelection();
            clickCount++;
        }
    }

    public void ClickReset()
    {
        clickCount = 0;
    }
}
