﻿using UnityEngine;

#if !UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine.Analytics;
#if UNITY_FACEBOOK
using Facebook.Unity;
#endif
//using GameAnalyticsSDK;
#endif

namespace SquareDino.CurrentAPIs
{
    public class MyAnalytics : MonoBehaviour
    {
        public static void LevelStart(int currentLevel)
        {
#if !UNITY_EDITOR
//        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level " + currentLevel);
#if UNITY_FACEBOOK
		FB.LogAppEvent ("game_start");
#endif
#endif
        }

        public static void LevelFailed(int currentLevel, int score = 0)
        {

#if !UNITY_EDITOR
#if UNITY_FACEBOOK
			FB.LogAppEvent ("game_end");
//			FB.LogAppEvent ("game_end_levels");
#endif
        Analytics.CustomEvent("Level Failed", new Dictionary<string, object> {{"Id", currentLevel}});
//        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level" + currentLevel, score);
#endif
        }

        public static void LevelWin(int currentLevel, int score = 0)
        {
#if !UNITY_EDITOR
#if UNITY_FACEBOOK
        FB.LogAppEvent ("game_end");
#endif
        Analytics.CustomEvent("Level Finished", new Dictionary<string, object> {{"Id", currentLevel}});
//        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level" + currentLevel, score);
#endif
        }
    
        public static void OnAdsShowInterstitial()
        {
#if !UNITY_EDITOR && UNITY_FACEBOOK
        FB.LogAppEvent ("ad_shown_interstitial");
#endif
        }
    
        public static void OnAdsShowBanner()
        {
#if !UNITY_EDITOR && UNITY_FACEBOOK
        FB.LogAppEvent ("banner_shown");
#endif
        }
    
        public static void OnAdsShowRewarded(bool finished)
        {
#if !UNITY_EDITOR && UNITY_FACEBOOK
        FB.LogAppEvent ("rewarded_shown: " + finished);
#endif
        }

        public static void NewHighScore(int value)
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("HighScore", new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}, {"score", value}});
//#endif
        }

        public static void RateClick()
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("RateFeedbackClick",
//            new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}});
//#endif
        }
    }
}